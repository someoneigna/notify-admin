﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public static class CacheDefinitions
    {
        public static readonly string Notifications = "notifications";
        public static readonly string ClientUser = "users";
    }
}