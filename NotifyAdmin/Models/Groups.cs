﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class Groups
    {
        [JsonProperty("owned_groups")]
        public List<Group> OwnedGroups { get; set; }
        [JsonProperty("subscription_groups")]
        public List<Group> SubscriptionGroups { get; set; }
    }
}