﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class JsonResponse
    {
        public string message { get; set; }
        public string id { get; set; }
    }
}