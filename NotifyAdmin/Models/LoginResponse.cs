﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class LoginResponse
    {
        public string id { get; set; }
        public string username { get; set; }
        public string message { get; set; }
        public string usertype { get; set; }
    }
}