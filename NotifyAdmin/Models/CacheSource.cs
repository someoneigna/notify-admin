﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace NotifyAdmin.Models
{
    public static class CacheSource
    {
        public static TimeSpan LifeExpectancy { get; set; }
        public static Dictionary<string, object> Cache = new Dictionary<string, object>();
        private static Dictionary<string, DateTime> lifeDate = new Dictionary<string, DateTime>();        

        internal static T Retrieve<T>(string key) where T : class
        {
            lock (Cache)
            {
                object value = null;
                Cache.TryGetValue(key, out value);
                if (value == null) return null;
                
                if (lifeDate[key].Millisecond - DateTime.UtcNow.Millisecond > LifeExpectancy.Milliseconds)
                {
                    Cache[key] = null;
                    return null;
                }
                return (T)value;
            }
        }

        internal static void Register<T>(string key, T value) where T : class
        {
            lock (Cache)
            {
                if (value == null) throw new NullReferenceException("Cant register a null value.");

                lifeDate[key] = DateTime.UtcNow;
                Cache[key] = value;
            }
        }

        internal static void Evict(string key)
        {
            lock (Cache)
            {                
                Cache[key] = null;
                lifeDate.Remove(key);
            }
        }
    }
}