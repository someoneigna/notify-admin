﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class NotificationList
    {
        public List<Notification> Result { get; set; }
    }
}