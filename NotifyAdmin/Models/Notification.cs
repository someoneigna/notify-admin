﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class Notification
    {
        [Required(ErrorMessage = "Se requiere un título")]
        [Display(Name = "Título")]
        [JsonProperty("name")]
        [DataType(DataType.MultilineText)]
        public string name { get; set; }

        [Required(ErrorMessage = "Se requiere un autor")]
        [Display(Name = "Autor")]
        [JsonProperty("author")]
        public string author { get; set; }

        public string id { get; set; }

        [Required(ErrorMessage = "Debes introducir contenido.")]
        [Display(Name = "Contenido")]
        [DataType(DataType.MultilineText)]
        public string content { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime creationDate { get; set; }

        [DataType(DataType.Date)]
        [JsonProperty("modificationDate")]
        public DateTime modificationDate { get; set; }
        
        public string group_id { get; set; }
        public Group group { get; set; }
    }
}