﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class Group
    {
        public string id { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Nombre")]
        [Required]
        public string name { get; set; }
        [DisplayName("Activo")]
        public bool active { get; set; }
        public bool IsPublic { get; set; }
    }
}