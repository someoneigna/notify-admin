﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Models
{
    public class ClientUser
    {
        public string id { get; set; }
        [Display(Name = "Usuario")]
        [Required]
        public string username { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string password { get; set; }
        [JsonProperty("creationDate")]
        public DateTime CreationDate { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Tipo")]
        public string usertype { get; set; }
        public List<Group> owned_groups { get; set; }
        public List<Group> subscription_groups { get; set; }

    }
}