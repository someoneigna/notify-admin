﻿using NotifyAdmin.Controllers.Errors;
using NotifyAdmin.Models;
using NotifyAdmin.Services;
using NotifyAdmin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotifyAdmin.Controllers
{
    public class GroupController : Controller
    {
        // GET: Group
        private NotifyAPIService service = new NotifyAPIService();
        /// <summary>
        /// Prepara la vista para ver todos los grupos si el usuario es admin sino solo vera sus grupos
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ClientUser userlog = (ClientUser)Session["userlog"];
            if (TempData["ErrorMessage"] != null)
                ViewBag.Message = TempData["ErrorMessage"];

            List<Group> values = null;

            if (userlog.usertype.Equals("admin"))
            {
                values = service.GetAllGroup();
            }
            else
            {
                values = service.GetUserGroups(userlog.id).OwnedGroups;
            }
            Group publicGroup = new Group();
            publicGroup.IsPublic = true;
            publicGroup.name = "Notificaciones publicas";
            publicGroup.active = true;
            values.Add(publicGroup);

            return View(values);
        }

        public ActionResult ListNotification(string id)
        {
            return RedirectToAction("Index", "Notification", new { id });
        }

        public ActionResult AddNotification()
        {
            return RedirectToAction("Add", "Notification");
        }
        /// <summary>
        /// Prepara la vista para modificar un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            List<Group> values = null;
            if (values == null)
            {
                values = service.GetAllGroup();
            }
            var results = values.Where<Group>(n => n.id == id);
            if (results == null)
            {
                TempData["ErrorMessage"] = String.Format(GroupControllerErrors.EditInvalidGroup);
                return RedirectToAction("Index");
            }

            Group value = results.Single();

            return View(value);
        }
        /// <summary>
        /// Guarda el grupo modificado
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,active")]Group model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    service.GroupSet(model);
                }
                catch (OperationFailedException)
                {
                    TempData["ErrorMessage"] = GroupControllerErrors.UpdatingGroupFailed;
                    return RedirectToAction("Index");
                }
                TempData["ErrorMessage"] = "Grupo actualizado correctamente.";

                return RedirectToAction("Index");
            }
            return View(model);
        }
        /// <summary>
        /// Desactiva un grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            try
            {
                Group group = service.GetGroup(id);
                string state;

                if (group.active)
                {
                    service.GroupRemove(id);
                    state = "desactivado";
                }
                else
                {
                    group.active = true;
                    service.GroupSet(group);
                    state = "activado";
                }

                TempData["ErrorMessage"] = "Grupo \'" + group.name + "\' " + state + " correctamente.";
            }
            catch (OperationFailedException ex)
            {
                TempData["ErrorMessage"] = String.Format(GroupControllerErrors.RemoveInvalidOrInexistant, ex.id);
            }

            return RedirectToAction("Index", "Group");
        }
        /// <summary>
        /// Prepara la vista para agregar un grupo
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }
        /// <summary>
        /// Guarda un grupo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult SaveNew(Group model)
        {
            ClientUser user = (ClientUser)Session["userlog"];
            if (!user.usertype.Equals("admin")){
                TempData["ErrorMessage"] = GroupControllerErrors.SavingNewGroupFailed;
                Response.Redirect("Home/Index");
            }
            try
            {
                service.GroupAdd(model);
            }
            catch (OperationFailedException)
            {
                TempData["ErrorMessage"] = GroupControllerErrors.SavingNewGroupFailed;
                Response.Redirect("Home/Index");
            }

            ViewData["Mensaje"] = "Grupo agregado correctamente";

            return RedirectToAction("Index");
        }
        /// <summary>
        /// Prepara la vista para agregar un admin al grupo
        /// </summary>
        /// <returns></returns>
        public ActionResult AddAdmin()
        {
            var vm = new AddGroupAdminViewModel();
            try
            {
                var user = (ClientUser)Session["userlog"];
                vm.Groups = new List<SelectListItem>();
                vm.Users = new List<SelectListItem>();

                if (user.usertype == "admin")
                {
                    service.GetAllGroup().ForEach(g =>
                    AddGroupIfActive(vm, g));
                    service.GetAllUsers().ForEach(u =>
                    AddUserIfGroupAdmin(vm, u));
                }
                else
                {
                    service.GetUserGroups(user.id)
                        .OwnedGroups
                        .ForEach(g => vm.Groups.Add(new SelectListItem()
                        {
                            Text = g.name,
                            Value = g.id
                        }));
                }
            }
            catch (OperationFailedException e)
            {
                throw new NullReferenceException("Error el usuario no se encuentra logeado.");
            }
            return View(vm);
        }

        private void AddGroupIfActive(AddGroupAdminViewModel vm, Group g)
        {
            vm.Groups.Add(new SelectListItem() { Text = g.name, Value = g.id });
        }
        private void AddUserIfGroupAdmin(AddGroupAdminViewModel vm, ClientUser u)
        {
            vm.Users.Add(new SelectListItem() { Text = u.username, Value = u.id });
        }
        /// <summary>
        /// Agrega un admin al grupo
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public ActionResult AdminNew(AddGroupAdminViewModel vm)
        {
            ClientUser user = (ClientUser)Session["userlog"];

            try
            {
                service.AddGroupAdmin(vm.SelectedUser.Value, vm.SelectedGroup.Value);
            }
            catch (OperationFailedException)
            {
                TempData["ErrorMessage"] = GroupControllerErrors.SavingNewGroupFailed;
                return View("Index");
            }

            ViewData["Mensaje"] = "Admin agregado correctamente a " + vm.SelectedGroup.Text;

            return View("Index");
        }
    }
}