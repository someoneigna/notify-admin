﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Controllers.Errors
{
    public static class HomeControllerErrors
    {
        public static readonly string EditInvalidNotification = "La notificación a editar es inválida o inexistente.";
        public static readonly string RemoveInvalidOrInexistant = "No se puede eliminar {0}, es inválida o inexistente.";
        public static readonly string SavingNewNotificationFailed = "La notificación creada no se pudo enviar al servidor.";
        public static readonly string UpdatingNotificationFailed = "La notificación no pudo ser actualizada en el servidor.";
    }
}