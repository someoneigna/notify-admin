﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Controllers.Errors
{
    public class GroupControllerErrors
    {
        public static readonly string EditInvalidGroup = "El grupo a editar es inválido o inexistente.";
        public static readonly string RemoveInvalidOrInexistant = "No se puede eliminar {0}, es inválido o inexistente.";
        public static readonly string SavingNewGroupFailed = "El Grupo creado no se pudo enviar al servidor.";
        public static readonly string UpdatingGroupFailed = "El grupo no pudo ser actualizado en el servidor.";
    }
}