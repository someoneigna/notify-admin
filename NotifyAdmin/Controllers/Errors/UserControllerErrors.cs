﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Controllers.Errors
{
    public class UserControllerErrors
    {
        public static readonly string EditInvalidUser = "El usuario a editar es inválido o inexistente.";
        public static readonly string RemoveInvalidOrInexistant = "No se puede eliminar {0}, es inválido o inexistente.";
        public static readonly string SavingNewUserFailed = "El usuario creado no se pudo enviar al servidor.";
        public static readonly string UpdatingUserFailed = "El usuario no pudo ser actualizado en el servidor.";
    }
}