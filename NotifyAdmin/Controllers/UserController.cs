﻿using NotifyAdmin.Controllers.Errors;
using NotifyAdmin.Models;
using NotifyAdmin.Services;
using NotifyAdmin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotifyAdmin.Controllers
{
    public class UserController : Controller
    {
        private NotifyAPIService service = new NotifyAPIService();
        // GET: User
        /// <summary>
        /// Prepara la vista para mostrar a todos los usuarios
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["userlog"] == null) return RedirectToAction("Login", "Login");
            ClientUser user = (ClientUser)Session["userlog"];
            if (user.usertype != "admin") return RedirectToAction("Index", "Group");
            if (TempData["ErrorMessage"] != null)
                ViewBag.Message = TempData["ErrorMessage"];

            List<ClientUser> values = null;
            try
            {
                values = service.GetAllUsers();

            }
            catch (OperationFailedException ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index", "Group");
            }

            if (values.Count == 0)
            {
                ViewData["Mensaje"] = "No hay usuarios que mostrar";

            }
            values.Reverse();
            return View(values);
        }
        /// <summary>
        /// Elimina un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            try
            {
                service.UserRemove(id);
                TempData["ErrorMessage"] = "User " + id;
                
            }
            catch (OperationFailedException ex)
            {
                TempData["ErrorMessage"] = String.Format(UserControllerErrors.RemoveInvalidOrInexistant, ex.id);
            }

            return RedirectToAction("Index", "User");

        }
        /// <summary>
        /// Prepara la vista para agregar un usuario
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            var vm = new AddUserViewModel();
            try
            {
                var user = (ClientUser)Session["userlog"];
                if (user.usertype.Equals("admin"))
                {
                    vm.usertypes.Add(new SelectListItem()
                    {
                        Text = "Administrador",
                        Value = "admin"
                    });
                    vm.usertypes.Add(new SelectListItem()
                    {
                        Text = "Coordinador",
                        Value = "group_admin"
                    });
                    vm.usertypes.Add(new SelectListItem()
                    {
                        Text = "Subscriptor",
                        Value = "subscriptor"
                    });
                }
                
            }
            catch (OperationFailedException e)
            {
                throw new NullReferenceException("Error el usuario no se encuentra logeado.");
            }
            return View(vm);
        }
        /// <summary>
        /// Guarda el usuario creado
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public ActionResult SaveNew(AddUserViewModel vm)
        {
            ClientUser user = (ClientUser)Session["userlog"];
            ClientUser model = null;
            if (vm.password.Equals(vm.passwordConfirm))
            {
                model = new ClientUser()
                {
                    username = vm.username,
                    password = vm.password,
                    email = vm.email,
                    usertype = vm.usertype
                };
            }
            try
            {
                service.UserAdd(model);
            }
            catch (OperationFailedException)
            {
                TempData["ErrorMessage"] = UserControllerErrors.SavingNewUserFailed;
                Response.Redirect("User/Index");
            }

            ViewData["Mensaje"] = "Usuario agregado correctamente a " + model.username;

            return RedirectToAction("Add");
        }
    }
}