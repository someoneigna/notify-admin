﻿using NotifyAdmin.Models;
using NotifyAdmin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotifyAdmin.Controllers
{
    public class LoginController : Controller
    {
        private NotifyAPIService service = new NotifyAPIService();
        // GET: Login
        public ActionResult Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.Message = TempData["ErrorMessage"];
            }
            return View("Login");
        }

        public ActionResult Login(string email, string password)
        {
            LoginResponse data = null;
            try
            {
                data = service.Login(email, password);
            }
            catch (OperationFailedException)
            {
                TempData["ErrorMessage"] = "error en login";
                return RedirectToAction("Index", "Login");
            }

            if (data.id != null)
            {
                if (data.usertype == "admin" || data.usertype == "group_admin") {
                    ClientUser vm = new ClientUser();
                    vm.id = data.id;
                    vm.username = data.username;
                    vm.usertype = data.usertype;
                    Session["userlog"] = vm;
                    return RedirectToAction("Index", "Group");
                }
                else
                {
                    ViewData["Mensaje"] = "Usted no tiene los permisos suficientes para acceder al sistema";
                    return View();
                }

            }
            else {
                ViewData["Mensaje"] = "Usuario o contraseña incorrecta";
                return View();
            }
            
        }
        public ActionResult Logout()
        {
            Session["userlog"] = null;
            return View("Login");
        }
    }
}