﻿using NotifyAdmin.Controllers.Errors;
using NotifyAdmin.Models;
using NotifyAdmin.Services;
using NotifyAdmin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace NotifyAdmin.Controllers
{
    public class NotificationController : Controller
    {
        private NotifyAPIService service = new NotifyAPIService();
        private string PublicGroupId = "PublicGroup";

        /// <summary>
        /// Prepara la vista para mostrar las notificaciones publicas o de un grupo en especifico
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(string id)
        {
            if (Session["userlog"] == null) return RedirectToAction("Login", "Login");

            if (TempData["ErrorMessage"] != null)
                ViewBag.Message = TempData["ErrorMessage"];

            List<Notification> values = null;
            try
            {
                if (id == null)
                {
                    values = service.GetAll();
                }
                else
                {
                    values = service.GetGroupNotifications(id);
                }

            }
            catch (OperationFailedException ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index");
            }

            if (values.Count == 0)
            {
                ViewData["Mensaje"] = "No hay notificaciones que mostrar";

            }
            values.Reverse();
            return View(values);
        }
        /// <summary>
        /// Prepara la vista para Agregar una notificacion
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            var vm = new AddNotificationViewModel();
            try
            {
                var user = (ClientUser)Session["userlog"];
                Group publicgroup = new Group();
                publicgroup.active = true;
                publicgroup.name = "Publico";
                publicgroup.id = PublicGroupId;
                vm.Groups = new List<SelectListItem>();

                if (user.usertype == "admin")
                {
                    service.GetAllGroup().ForEach(g => AddGroupIfActive(vm, g));                   
                }
                else
                {
                    service.GetUserGroups(user.id)
                        .OwnedGroups
                        .ForEach(g => vm.Groups.Add(new SelectListItem()
                    {
                        Text = g.name,
                        Value = g.id
                    }));
                }
                vm.Notification = new Notification();
                vm.Groups.Add(new SelectListItem()
                {
                    Text = publicgroup.name,
                    Value = publicgroup.id
                });
            }
            catch (OperationFailedException e)
            {
                throw new NullReferenceException("Error el usuario no se encuentra logeado.");
            }
            return View(vm);
        }

        private void AddGroupIfActive(AddNotificationViewModel vm, Group g)
        {
            if (!g.active) return;
            vm.Groups.Add(new SelectListItem()
            {
                Text = g.name,
                Value = g.id
            });
        }
        /// <summary>
        /// Prepara la vista para modificar una notificacion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            Notification value = service.Get(id);
            return View(value);
        }
        /// <summary>
        /// Elimina una notificacion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            Notification model = service.Get(id);
            try
            {
                service.Remove(id);
                TempData["ErrorMessage"] = "Notificacion borrada satisfactoriamente.";
            }
            catch(OperationFailedException ex)
            {
                TempData["ErrorMessage"] = String.Format(HomeControllerErrors.RemoveInvalidOrInexistant, ex.id);
            }
            
            return RedirectToAction("Index", new { id = model.group_id });
        }

        /// <summary>
        /// Guarda la notificacion editada
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,name,content,author,group_id")]Notification model)
        {
            try
            {
                service.Set(model);
            }
            catch (OperationFailedException)
            {
                TempData["ErrorMessage"] = HomeControllerErrors.UpdatingNotificationFailed;
                return RedirectToAction("Index");
            }
            TempData["ErrorMessage"] = "Notificacion actualizada correctamente.";
            
            return RedirectToAction("Index", new { id = model.group_id });
        }
        /// <summary>
        /// Guarda una nueva notificacion
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public ActionResult SaveNew(AddNotificationViewModel vm)
        {

            Group group = null;

            if (vm.Notification.group.id != PublicGroupId)
            {
                group = service.GetGroup(vm.Notification.group.id);
            }

            ClientUser user = (ClientUser)Session["userlog"];
            Notification model = new Notification() {
                name = vm.Notification.name,
                content = vm.Notification.content,
                group = group,
                author = user.username
            };

            try
            {
                service.Add(model);
            }
            catch (OperationFailedException)
            {
                TempData["ErrorMessage"] = HomeControllerErrors.SavingNewNotificationFailed;
                Response.Redirect("Home/Index");
            }

            string groupName = (group == null ? "Grupo publico" : group.name);
            ViewData["Mensaje"] = "Notificacion agregada correctamente a " + groupName;

            return RedirectToAction("Index", "Group");
        }
    }
}