﻿using Newtonsoft.Json;
using NotifyAdmin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotifyAdmin.ViewModels
{
    public class AddUserViewModel
    {
        public string id { get; set; }
        [Display(Name = "Usuario")]
        public string username { get; set; }
        [Display(Name = "Password")]
        public string password { get; set; }
        [Display(Name = "Confirmación Contraseña")]
        public string passwordConfirm { get; set; }
        [JsonProperty("creationDate")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Tipo")]
        public string usertype { get; set; }
        public List<SelectListItem> usertypes { get; set; }

        public AddUserViewModel()
        {
            usertypes = new List<SelectListItem>();
        }
    }
}