﻿using NotifyAdmin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotifyAdmin.ViewModels
{
    public class AddGroupAdminViewModel
    {
        public List<SelectListItem> Users { get; set; }
        [Display(Name = "User")]
        public SelectListItem SelectedUser { get; set; }
        public List<SelectListItem> Groups { get; set; }
        [Display(Name = "Grupo")]
        public SelectListItem SelectedGroup { get; set; }
    }
}