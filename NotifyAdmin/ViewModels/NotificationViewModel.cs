﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NotifyAdmin.ViewModels
{
    public class NotificationViewModel
    {     
        [Required(ErrorMessage = "Se requiere un título")]
        [Display(Name = "Título")]
        public string Name { get; set; }
        

        [Required(ErrorMessage = "Debes introducir contenido.")]
        [Display(Name = "Contenido")]
        public string Content { get; set; }


        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Display(Name = "Fecha Creación")]
        public string CreationDate { get; set; }
    }
}