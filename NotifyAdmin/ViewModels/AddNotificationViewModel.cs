﻿using NotifyAdmin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotifyAdmin.ViewModels
{
    public class AddNotificationViewModel
    {
        public List<SelectListItem> Groups { get; set; }
        public Notification Notification { get; set; }
        [Display(Name = "Grupo")]
        public SelectListItem SelectedGroup { get; set; }
    }
}