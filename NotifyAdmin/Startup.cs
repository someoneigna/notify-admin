﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NotifyAdmin.Startup))]
namespace NotifyAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
