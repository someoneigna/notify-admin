﻿using NotifyAdmin.Models;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;

namespace NotifyAdmin.Services
{
    public class NotifyAPIService
    {
        private string serviceKey = "4f081f27-8247-4f42-94dc-2097f8404f4d";
        private string password = "n0t1fy4dm1n";
        public static readonly string LocalHost = "http://127.0.0.1:5000";
        private const string AddAction = "/add";
        private const string SetAction = "/set";
        private const string GetAllAction = "/all";
        private const string GetAction = "/get";
        private const string RemoveAction = "/remove";
        private const string UserUrl = "/user";
        private const string GroupUrl = "/group";
        private const string SuscriptionUrl = "/subscription";
        private const string LoginUrl = "/login";
        private const string NotificationUrl = "/notification";
        private readonly string AddGroupAdminAction = "/addadmin";

        public string Server { get; set; }

        public NotifyAPIService()
        {
            Server = LocalHost;
        }

        public LoginResponse Login(string email_, string pass)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(UserUrl + LoginUrl, Method.POST);
            request.AddHeader("Content-Type", "text/json");
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { email = email_, password = pass});

            var response = client.Execute<LoginResponse>(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to login:" + Server);
            }
            return response.Data;     
        }

        public List<ClientUser> GetAllUsers()
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(UserUrl + GetAllAction, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<List<ClientUser>>(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to get users:" + Server);
            }
            return response.Data;
        }

        public ClientUser GetUser(string id)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(UserUrl +  GetAction + "/" + id, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<ClientUser>(request);

            if (response.Data == null) response.Data = new ClientUser();
            return response.Data;
        }

        public void UserAdd(ClientUser value)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(UserUrl + AddAction, Method.POST);
            request.AddHeader("Content-Type", "text/json");
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(value);

            var response = client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to send user to server:" + Server);
            }
        }

        public void UserRemove(string id)
        {
            var client = new RestClient(LocalHost);
            var deserializer = new JsonDeserializer();

            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", deserializer);

            var request = new RestRequest(UserUrl + RemoveAction + "/" + id, Method.DELETE);

            request.RequestFormat = DataFormat.Json;
            var response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new OperationFailedException("Failed to remove user in server:" + response.ErrorMessage, id);
            }
        }
        
        public List<Notification> GetAll()
        {           
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(GetAllAction, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<List<Notification>>(request);
            
            if (response.Data == null) response.Data = new List<Notification>();           
            return response.Data;
        }

        public void Add(Notification value)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(AddAction, Method.POST);
            request.AddHeader("Content-Type", "text/json");
            request.JsonSerializer = new NotificationCustomSerializer();
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(value);

            var response = client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to send notification to server:" + Server);
            }
        }

        public Notification Get(string id)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(GetAction + "/" + id, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<Notification>(request);

            if (response.Data == null) response.Data = new Notification();
            return response.Data;
        }


        public void Set(Notification value)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(SetAction, Method.POST);
            request.AddHeader("Content-Type", "text/json");            
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(value);

            var response = client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to update notification on server:" + Server);
            }
        }

        public void Remove(string id)
        {
            var client = new RestClient(LocalHost);
            var deserializer = new JsonDeserializer();            

            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", deserializer);

            var request = new RestRequest(RemoveAction + "/" + id, Method.DELETE);
            
            request.RequestFormat = DataFormat.Json;                                              
            var response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK) {
                throw new OperationFailedException("Failed to remove notification in server:" + response.ErrorMessage, id);
            }
        }

        public List<Group> GetAllGroup()
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(GroupUrl + GetAllAction, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<List<Group>>(request);

            if (response.Data == null) response.Data = new List<Group>();
            return response.Data;
        }

        public Group GetGroup(string id)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(GroupUrl + GetAction +"/" + id, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<Group>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new OperationFailedException("Failed to get group data from server:" + response.ErrorMessage, id);
            }
            return response.Data;
        }

        public Groups GetUserGroups(string id)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(UserUrl + GroupUrl + "s/" + id, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<Groups>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new OperationFailedException("Failed to get user groups from server:" + response.ErrorMessage, id);
            }
                        
            return response.Data;
        }

        public List<Notification> GetGroupNotifications(string id)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(NotificationUrl + GroupUrl + "/" + id, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<List<Notification>>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new OperationFailedException("Failed to get group notifications from server:" + response.ErrorMessage, id);
            }

            return response.Data;
        }

        public void GroupAdd(Group value)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(GroupUrl + AddAction, Method.POST);
            request.AddHeader("Content-Type", "text/json");
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(value);

            var response = client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to send group to server:" + Server);
            }
        }

        public void GroupSet(Group value)
        {
            var client = new RestClient(LocalHost);
            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", new JsonDeserializer());
            var request = new RestRequest(GroupUrl + SetAction, Method.POST);
            request.AddHeader("Content-Type", "text/json");
            request.RequestFormat = DataFormat.Json;
            request.JsonSerializer = new GroupCustomSerializer();
            request.AddJsonBody(value);

            var response = client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new OperationFailedException("Failed to update group on server:" + Server);
            }
        }

        public void GroupRemove(string id)
        {
            var client = new RestClient(LocalHost);
            var deserializer = new JsonDeserializer();

            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", deserializer);

            var request = new RestRequest(GroupUrl + RemoveAction + "/" + id, Method.DELETE);

            request.RequestFormat = DataFormat.Json;
            var response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new OperationFailedException("Failed to remove group in server:" + response.ErrorMessage, id);
            }
        }

        public void AddGroupAdmin(string idUser, string idGroup)
        {
            var client = new RestClient(LocalHost);
            var deserializer = new JsonDeserializer();

            client.Authenticator = new HttpBasicAuthenticator(serviceKey, password);
            client.AddHandler("*", deserializer);

            var request = new RestRequest(GroupUrl + AddGroupAdminAction, Method.POST);

            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { user_id = idUser, group_id = idGroup });
            var response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new OperationFailedException("Failed to send notification to server:" + Server);
            }
        }
    }
}