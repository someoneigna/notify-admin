﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NotifyAdmin.Models;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NotifyAdmin.Services
{
    public class GroupCustomSerializer : ISerializer
    {
        public string ContentType
        {
            get { return "application/json"; }
            set { }
        }

        public string DateFormat
        {
            get; set;
        }

        public string Namespace { get; set; }

        public string RootElement { get; set; }
        public string Serialize(object obj)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new DefaultContractResolver() };
            Group value = (Group)obj;
           
            return toJson(new string[] { "id", "name", "active" }, new object[]{ value.id, value.name, value.active});
        }

        private string toJson(string[] keys, object[] values)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("{");

            for (int i = 0; i < keys.Length; i++)
            {
                string valueStr = values[i] is string ? "\"" + values[i] + "\"" : values[i].ToString().ToLower();
                builder.Append("\"" + keys[i] + "\":" + valueStr);
                if (i + 1 < keys.Length)
                {
                    builder.Append(",");
                }
            }
            builder.Append("}");
            return builder.ToString();
        }
    }
}