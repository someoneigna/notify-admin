﻿using NotifyAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Services
{
    public class OperationFailedException : Exception
    {
        public string id;
        public OperationFailedException(string errorMessage, string id) : base(errorMessage)
        {
            this.id = id;   
        }
        
        public OperationFailedException(string errorMessage) : base(errorMessage)
        {
        }        
    }
}