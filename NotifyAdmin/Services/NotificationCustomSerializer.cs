﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NotifyAdmin.Models;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotifyAdmin.Services
{
    public class NotificationCustomSerializer : ISerializer
    {
        public string ContentType
        {
            get { return "application/json"; }
            set { }
        }

        public string DateFormat
        {
            get; set;
        }

        public string Namespace { get; set; }

        public string RootElement { get; set; }
        public string Serialize(object obj)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new DefaultContractResolver() };
            Notification value = (Notification)obj;

            if (value.group == null)
            {
                return JsonConvert.SerializeObject(serializePublicNotification(value), Formatting.None, settings);
            }
            return JsonConvert.SerializeObject(serializeGroupNotification(value), Formatting.None, settings);
        }

        private object serializeGroupNotification(Notification value)
        {
            return new
            {
                name = value.name,
                author = value.author,
                group = value.group.id,
                content = value.content
            };

        }

        private object serializePublicNotification(Notification value)
        {
            return new
            {
                name = value.name,
                author = value.author,
                content = value.content
            };

        }

    }
}